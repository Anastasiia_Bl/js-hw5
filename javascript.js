
// Варіант 4 - звичайна функція 

function createNewUser() {

  let firstName = prompt("Ваше ім'я");
  let lastName = prompt("Ваше прізвище");

    const newUser = {
      _firstName: firstName,
      _lastName: lastName,

      set firstName(value) {
        this._firstName = value;
      },
      get firstName() {
        return this._firstName;
      },

      set lastName(value) {
        this._lastName = value;
      },
      get lastName() {
        return this._lastName;
      },

      getLogin: function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      }
    }
    return newUser;
  }
  let user = createNewUser();

  console.log(user);
  console.log(user.getLogin());






  // Варіант 1 - без функції, просто як Об'єкт - працює

//   const newUser = {
//   firstName: "",
//   lastName: "",
//   getLogin: function() {
//     return this.firstName[0] + this.lastName;
//   }
// }

// newUser.firstName = prompt("Ваше ім'я");
// newUser.lastName = prompt("Ваше прізвище");

// console.log(newUser.getLogin());





// Варіант 2 - з Функцією але працює не зовсім корректно, 2 рази питає Прізвище та ім'я, не з малої букви ініціали

// function createNewUser(firstName, lastName) {

// firstName = prompt("Ваше ім'я");
// lastName = prompt("Ваше прізвище");

//     return {
//     firstName: firstName,
//     lastName: lastName,
//     getLogin: function() {
//       return this.firstName[0] + this.lastName;
//     }
//   }
// }
// const newUser = createNewUser();
// console.log(createNewUser());
// console.log(newUser.getLogin());





// Варіант 3 - способ создание объекта через конструктор, https://dmitrytinitilov.gitbooks.io/strange-javascript/content/oop/objects.html

// function createNewUser(firstName, lastName) {

//   firstName = prompt("Ваше ім'я");
//   lastName = prompt("Ваше прізвище");

//   this.firstName = firstName.toLowerCase();
//   this.lastName = lastName.toLowerCase();
  
//   this.getLogin = function() {
//         return this.firstName[0] + this.lastName;
//       }
//   }
//   const newUser = new createNewUser();
//   console.log(newUser);
//   console.log(newUser.getLogin());

